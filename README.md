#lib-android-LoadingLayout

Android 业务开发常用的 loadinglayout 用于封装`加载过程中`,`加载完成&有数据`,`加载完成&无数据`,`加载错误`的情况。

有任何建议或反馈 [请联系: chenjunqi.china@gmail.com](mailto:chenjunqi.china@gmail.com) 

欢迎大家加入`android 开源项目群(369194705)`, 有合适的项目大家一起 `fork`;

# 注意
下处声明了ids 用于绑定事件，在对应的布局中需要与 ids 保持一致。

```

    <item name="loading_retry" type="id"/>
    <item name="loading_progress" type="id"/>

```

 对应布局代码
 ```
 
     <TextView android:id="@+id/loading_retry"
               android:layout_width="match_parent"
               android:layout_height="match_parent"
               android:background="@android:color/white"
               android:gravity="center"
               android:text="EMPTY(Click Retry)"
               android:textSize="30sp"/>
 
 
 ```
 
# 使用方法

1) 定义布局 绑定不同的 layout

```

    <com.bookbuf.library.LoadingLayout
        android:id="@+id/loadingLayout"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="#F3F929"
        app:content="@layout/content_content"
        app:empty="@layout/content_empty"
        app:error="@layout/content_error"
        app:loading="@layout/content_loading">

    </com.bookbuf.library.LoadingLayout>
    
    

```

2) 为 `loadinglayout` 绑定 `adapter`

```

		/*bind view*/
		loadingLayout = (LoadingLayout) findViewById (R.id.loadingLayout);
		adapter = new SimpleAdapter ();
		loadingLayout.setAdapter (adapter);
		loadingLayout.setRetryListener (new View.OnClickListener () {
			@Override
			public void onClick (View v) {
				adapter.bindLoading ();
				// 耗时操作 ...
				adapter.bindError();
			}
		});

```

3) 图例

[点击这里查看截屏](video.mp4)

----------------

求问：内嵌 mp4 markdown 语法是神马？


