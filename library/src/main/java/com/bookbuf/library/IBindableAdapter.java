package com.bookbuf.library;

/**
 * Created by robert on 16/6/6.
 */
public interface IBindableAdapter<Data> {

	void bind (Data data);

	boolean isEmpty ();

	void unbind ();
}
